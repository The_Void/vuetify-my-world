let initialState = () => {
    return {
        first_name: '',
        last_name: '',
        phone_number: '',
        email: '',
        state: '',
        terms_and_conditions: ''
    }
};

export const user = {
    namespaced: true,
    state: initialState,
    mutations: {
        UPDATE_FIRST_NAME(state, payload) {
            state.first_name = payload;
        },
        UPDATE_LAST_NAME(state, payload) {
            state.last_name = payload;
        },
        UPDATE_PHONE_NUMBER(state, payload) {
            state.phone_number = payload;
        },
        UPDATE_EMAIL(state, payload) {
            state.email = payload;
        },
        UPDATE_STATE(state, payload) {
            state.state = payload;
        },
        UPDATE_TERMS_AND_CONDITIONS(state, payload) {
            state.terms_and_conditions = payload;
        },
        RESET_STATE(state) {
            const cloned_state = initialState();

            Object.keys(cloned_state).forEach(key => {
                state[key] = cloned_state[key];
            })
        }  
    }
}