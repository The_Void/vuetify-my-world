import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import './validation/vee-validate';
import VueAnalytics from 'vue-analytics';

Vue.config.productionTip = false;

const isProd = process.env.NODE_ENV === 'production';

const state = Vue.observable({ vga_id: 'UA-152749848-1' });

const mutations = {
  setGa(id) {
    state.vga_id = id;
  }
};

const load_plugin = (() => {
  Vue.use(VueAnalytics, {
    id() {
      return state.vga_id;
    },
    router,
    debug: {
      enabled: !isProd,
      sendHitTask: !isProd
    },
    autoTracking: {
      exception: true
    }
  });
});

window.api_key = ((id) => {
  try {
    console.log('Setting GA ID: ', id);
    mutations.setGa(id);

    load_plugin();
  } catch (error) {
    console.log('No id assigned to window...setting default', 'error: ', error);
    
  }
});

Vue.use(VueAnalytics, {
  id() {
    return state.vga_id;
  },
  router,
  debug: {
    enabled: !isProd,
    sendHitTask: !isProd
  },
  autoTracking: {
    exception: true
  }
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')
